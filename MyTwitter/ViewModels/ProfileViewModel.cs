﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyTwitter.Models;

namespace MyTwitter.ViewModels
{
    public class ProfileViewModel
    {
        public ApplicationUser User { get; set; }
        public List<Post> Posts { get; set; }
    }
}
