﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MyTwitter.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public IEnumerable<Post> Posts { get; set; }

        public bool IsBlocked { get; set; }

        [NotMapped]
        public List<string> Roles { get; set; }

        public IEnumerable<UserChatConnection> UserChatConnections { get; set; }
    }
}
