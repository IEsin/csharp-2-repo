﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTwitter.Models
{
    public class Chat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }

        public IEnumerable<UserChatConnection> UserChatConnections { get; set; }
        public IEnumerable<ChatMessage> Messages { get; set; }
    }
}
