﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyTwitter.Data;
using MyTwitter.Models;
using MyTwitter.ViewModels;

namespace MyTwitter.Controllers
{
    public class ChatController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ChatController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Start(string userId)
        {
            ApplicationUser currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
            ApplicationUser user = await _userManager.FindByIdAsync(userId);

            Chat newChat = _context.Chats.Add(new Chat()
            {
                Name = user.UserName
            }).Entity;

            UserChatConnection myConnection = new UserChatConnection()
            {
                Chat = newChat,
                User = currentUser
            };

            UserChatConnection userConnection = new UserChatConnection()
            {
                Chat = newChat,
                User = user
            };

            _context.UserChatConnections.AddRange(myConnection, userConnection);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", new {id = newChat.Id});
        }

        public async Task<IActionResult> Index(int id)
        {
            ApplicationUser currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
            Chat chat = _context.Chats
                .Include(c => c.Messages)
                .Include(c => c.UserChatConnections)
                .ThenInclude(u => u.User)
                .FirstOrDefault(c => c.Id == id);
            List<ApplicationUser> users = chat.UserChatConnections.Select(u => u.User).ToList();
            List<ChatMessage> messages = chat.Messages.OrderBy(m => m.MessageDate).ToList();

            ChatViewModel model = new ChatViewModel()
            {
                Chat = chat,
                Users = users,
                CurrentUserId = currentUser.Id,
                Messages = messages
            };

            return View(model);
        }

        public async Task<IActionResult> Send(int chatId, string newMessage)
        {
            ChatMessage message = new ChatMessage()
            {
                Chat = _context.Chats.FirstOrDefault(c => c.Id == chatId),
                Author = await _userManager.FindByEmailAsync(User.Identity.Name),
                Content = newMessage,
                MessageDate = DateTime.Now,
            };

            await _context.ChatMessages.AddAsync(message);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", new {id = chatId});
        }

        public async Task<IActionResult> Direct()
        {
            ApplicationUser currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);

            List<Chat> chats = _context.UserChatConnections
                .Include(u => u.Chat)
                .Where(u => u.UserId == currentUser.Id)
                .Select(u => u.Chat).ToList();

            return View(chats);
        }

        public async Task<string> AddUsers(string[] users, string chatId)
        {
            Chat chat = _context.Chats.FirstOrDefault(c => c.Id.ToString() == chatId);

            foreach (string userName in users)
            {
                ApplicationUser user = await _userManager.FindByNameAsync(userName);
                UserChatConnection connection = new UserChatConnection()
                {
                    Chat = chat,
                    User = user
                };
                _context.UserChatConnections.Add(connection);
            }

            
            await _context.SaveChangesAsync();
            List<string> chatUsers = _context.UserChatConnections.Where(u => u.ChatId.ToString() == chatId)
                .Select(u => u.User).Select(u => u.UserName).ToList();
            return string.Join(" ", chatUsers);
        }

        
    }
}